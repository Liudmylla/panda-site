import React from "react"
import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"

import { rhythm } from "../utils/typography"
export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )
  return (
    <div
      css={css`
        margin: 0 auto;
        max-width: 1200px;
        padding: ${rhythm(2)};
        padding-top: ${rhythm(1.5)};
      `}
    >
      <Link to={`/`}>
        <h3
          css={css`
            margin-bottom: ${rhythm(1.5)};
            display: inline-block;
            font-style: normal;
          `}
        >
          {data.site.siteMetadata.title}
        </h3>
      </Link>

      <Link
        to={`/galery/`}
        css={css`
          padding: ${rhythm(1.5)};
          text-decoration: none;
        `}
      >
        Galery
      </Link>
      <Link
        to={`/contact/`}
        css={css`
          padding: ${rhythm(1.5)};
          text-decoration: none;
        `}
      >
        Contacts
      </Link>

      {children}
    </div>
  )
}
